# Tardis

TARDIS is a module that allows users to go back in time and visit nodes from
the past. You can see nodes per year and month, using a View display that
creates links automatically, filtered in the format example.com/archive/YYYY/MM.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/tardis).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/tardis).

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as any other contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module ships with a base View called _TARDIS_, containing a block and a page
display.

### Block display

The block is set to use the _TARDIS_ format. Click on _Settings_ to fine-tune
its options:
- _Link path_: the desired base URL for your archive. For example, if you want
your links presented as `example.com/archive/YYYY/MM`, enter "archive" here.
- _Month date format_: use PHP's [Date format](https://www.php.net/manual/en/datetime.format.php)
to customize how month links are displayed. For example, enter "F/Y" and you'll
see links like "November/1963".
- _Nesting_: When set to "yes", will display month links as sub-items of years.
- _CSS classes_: extra CSS classes for theming.

### Page display

A simple page which takes a _year_ and a _month_ as arguments to filter posts.

Customize its settings such as which content to display, view mode etc. to your
liking.

Please keep in mind this page's address must match the _Link path_ seen above.

### Block placement

Once the _TARDIS_ View has been tailored to your needs, visit **/admin/structure/block**
and, under the desired theme section, click on _Place block_. Look for the
_TARDIS_ one and click on the _Place block_ button next to it. Adjust block
visibility; for example, leave it only on your archive pages, eg.
```
/archive
/archive/*
```

After these steps are concluded, the module is ready to use.

## Maintainers

- Luciano Cossich Sales - [luco](https://www.drupal.org/u/luco)
